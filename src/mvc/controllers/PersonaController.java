// nombre del paquete al que pertenece esta clase.
package mvc.controllers;
// paquetes externos.
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import javax.swing.JTable;
// paquetes internos.
import mvc.models.PersonaModel;
import mvc.views.PersonaView;
import mvc.views.utils.Alerta;

/**
 * Clase que implementa a las intefaces (ActionListener, MouseListener).
 * Esta clase sirve para controlar la interacción entre PersonaView
 * y PersonaModel. Cualquier acción que necesite ejecutar alguna
 * combinación de las dos clases mencionadas, pasará por
 * esta clase.
 *
 * 
 * @author luisherrera
 */
public class PersonaController extends Controller{
    // atributos de la clase conttrolador.
    PersonaView vista;
    PersonaModel modelo;

    /**
     * Al momento de ejecutar la acción que inicia la entidad persona, se
     * debe iniciar el controlador entregando nuevas instancias de
     * la vista y el modelo que se controlarán desde acá.
     * 
     * @param vista
     * @param modelo 
     */
    public PersonaController(PersonaView vista, PersonaModel modelo) {
        this.vista = vista;
        this.modelo = modelo;

    }
    
    /**
     * Inicia el controlador, es decir, ejecuta las acciones necesarias
     * para que el controlador parta su ejecución.
     */
    public void iniciar(){
        // asigna los componentes de acción a los Listener que los ejecutan.
        vista.jbMantPerGuardar.addActionListener(this);
        vista.jtablePersonas.addMouseListener(this);
        vista.jbActualizar.addActionListener(this);
        vista.jButtonEliminar.addActionListener(this);
        // llena la grilla de la vista con las personas en la base de datos.
        listaPersonasEnTabla();

    }

    /**
     * Ejecuta todas las tareas necesarias para guardar una persona en la BDD.
     */
    public void procesoDeGuardado(){
        guardaPersona();
        listaPersonasEnTabla();
        
    }
    
    /**
     * Ejecuta las tareas para que los datos de la persona sea actualizados.
     */
    public void procesoDeActualizado(){
        actualizarPersona();
        listaPersonasEnTabla();
        restauraFormulario();
    }
    
    /**
     * Ejecuta las tareas para que sea eliminado un registro de personas.
     */
    public void procesoDeEliminado(){
        eliminarPersona();
        listaPersonasEnTabla();
        restauraFormulario();
    }
    
    public void eliminarPersona(){
        // validamos la acción
        int confirma = Alerta.confimacion(vista, 
                modelo, 
                Alerta.CONFIRMAR_ELIMINAR
        );
        
        if(confirma == 0){
            seteaValoresModelo();
            if(modelo.delete() > 0)
                Alerta.informacion(vista, 
                        modelo, 
                        Alerta.MENSAJE, 
                        "Datos eliminados correctamente."
                );
            else
                Alerta.informacion(vista, 
                        modelo, 
                        Alerta.MENSAJE_ERROR, 
                        "Ocurrió un error al eliminar."
                );
        }
    }
    
    public void actualizarPersona(){
        // validamos la acción
        int confirma = Alerta.confimacion(vista, 
                modelo, 
                Alerta.CONFIRMAR_ACTUALIZAR
        );
        
        if(confirma == 0){
            seteaValoresModelo();

        if(modelo.put() > 0)
                Alerta.informacion(vista, 
                        modelo, 
                        Alerta.MENSAJE, 
                        "Datos actualizados correctamente."
                );
            else
                Alerta.informacion(vista, 
                        modelo, 
                        Alerta.MENSAJE_ERROR, 
                        "Ocurrió un error al actualizar."
                );
        }
        
    }
    
    // INICIO DE LOS HELPERS
    
    /**
     * deja el formulario en su estado inicial.
     */
    public void restauraFormulario(){
        vista.jbActualizar.setEnabled(false);
        vista.jButtonEliminar.setEnabled(false);
        vista.jbMantPerGuardar.setEnabled(true);
        vista.vaciaFormulario();
        vista.jtPersonaId.setText("");
        
    }
    
    /**
     * Toma los valores que están en el formulario y los pone en el modelo.
     * 
     */
    public void seteaValoresModelo(){
        String nombre = vista.jtMantPerNombre.getText();
        String apellido = vista.jtMantPerApellido.getText();
        String direccion = vista.jtMantPerDireccion.getText();
        String cargo = vista.jtMantPerCargo.getText();
        
        modelo.setNombre(nombre);
        modelo.setApellido(apellido);
        modelo.setDireccion(direccion);
        modelo.setCargo(cargo);
    }

    /**
     * guarda los datos del formulario en la base de datos.
     */
    public void guardaPersona(){
        seteaValoresModelo();
        
        if(modelo.post() > 0)
            Alerta.informacion(vista, 
                    modelo, 
                    Alerta.MENSAJE, 
                    "Datos ingresados correctamente."
            );
        else
            Alerta.informacion(vista, 
                    modelo, 
                    Alerta.MENSAJE_ERROR, 
                    "Ocurrió un error al ingresar."
            );

    }
    
    /**
     * Llena la tabla en la vista, con los datos en la base de datos.
     */
    public void listaPersonasEnTabla(){
        vista.jtablePersonas.setModel(modelo.getAllTableModel());
        
    }
    
    /**
     * Método encargado de llenar los campos de texto.
     */
    public void pasaDatosAFormulario(){
        vista.jtPersonaId.setText(modelo.getId());
        vista.jtMantPerNombre.setText(modelo.getNombre());
        vista.jtMantPerApellido.setText(modelo.getApellido());
        vista.jtMantPerDireccion.setText(modelo.getDireccion());
        vista.jtMantPerCargo.setText(modelo.getCargo());

    }
    
    /**
     * Controla los valores nulos que se traen de la base de datos.
     * en caso ser null, este le asigna un String "nulo".
     * 
     * @param str
     * @return 
     */
    public String controlaNulos(Object str){
        if (str == null) str = "null";

        return str.toString();
    }
    
    /**
     * Habilita los botones de actualizar y eliminar.
     * inhabilita el de guardar.
     * 
     */
    public void prepararBotones(){
        vista.jbMantPerGuardar.setEnabled(false);
        vista.jbActualizar.setEnabled(true);
        vista.jButtonEliminar.setEnabled(true);
    }
    
    public void cargaDatosTablaAModelo(MouseEvent ex){
        JTable table = (JTable) ex.getSource();
             
        // captura los valores de la fila
        Object id = table.getValueAt(table.getSelectedRow(), 0);
        Object nombre = table.getValueAt( table.getSelectedRow(), 1);
        Object apellido = table.getValueAt(table.getSelectedRow(), 2);
        Object direccion = table.getValueAt(table.getSelectedRow(), 3);
        Object cargo = table.getValueAt(table.getSelectedRow(), 4);

        //Seteamos la persona en el modelo.
        modelo.setId(controlaNulos(id));
        modelo.setNombre(controlaNulos(nombre));
        modelo.setApellido(controlaNulos(apellido));
        modelo.setDireccion(controlaNulos(direccion));
        modelo.setCargo(controlaNulos(cargo));
    }

    
    // INICIO DE EVENTOS
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == vista.jbMantPerGuardar) procesoDeGuardado();
        if(e.getSource() == vista.jbActualizar) procesoDeActualizado();
        if(e.getSource() == vista.jButtonEliminar) procesoDeEliminado();
        
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        // evento de click en la tabla personas.
        if(e.getSource() == vista.jtablePersonas){
             cargaDatosTablaAModelo(e);
             pasaDatosAFormulario();
             prepararBotones();

        }

    }
    
}
