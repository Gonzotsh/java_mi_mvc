package mvc.controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Esta clase "implementa" las interfaces Listeners, para no tener que 
 * tener métodos vacíos en los controladores y solamente sobrescribir
 * aquellos métodos que vamos a utilizar.
 * 
 * Por ejemplo al implementar MouseListener, este posee cuatro métodos.
 * mousePressed, mouseReleased, mouseEntered, mouseExited. Pero
 * tan solo queremos ocupar uno.
 * @author luisherrera
 */
public class Controller implements ActionListener, MouseListener{
    
    @Override
    public void actionPerformed(ActionEvent e) {}

    @Override
    public void mouseClicked(MouseEvent e) {}

    @Override
    public void mousePressed(MouseEvent e) {}

    @Override
    public void mouseReleased(MouseEvent e) {}

    @Override
    public void mouseEntered(MouseEvent e) {}

    @Override
    public void mouseExited(MouseEvent e) {}
    
}
