/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mvc.views;

/**
 *
 * @author luisherrera
 */
public class HomeView extends javax.swing.JPanel {

    /**
     * Creates new form Home
     */
    public HomeView() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jlTitulo = new javax.swing.JLabel();

        setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.RIGHT));

        jlTitulo.setFont(new java.awt.Font("Lucida Grande", 1, 18)); // NOI18N
        jlTitulo.setText("Bienvenido a Futuro K-Paz");
        add(jlTitulo);
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JLabel jlTitulo;
    // End of variables declaration//GEN-END:variables
}
